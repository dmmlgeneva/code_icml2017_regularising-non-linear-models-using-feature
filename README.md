# My project's README
This is the code for the paper "Regularising Non-linear Models Using Feature Side-information"  
[Link to the paper](http://proceedings.mlr.press/v70/mollaysa17a.html)

The directory includes code for 4 models (The two models introduced in the paper and two baseline models)  
1. Analytical_Approach: our model where the analyatical approximation of our regularizer is applied.  
2. Stochastic_Approach: our model where the data augmentation is used to approximate the regularizer.  
3. NN_l2: baseline model where there is only l2 regularizer is applied on NN (no side information is provided)  
4. NN_dropout: baseline model where there is only dropout regularizer is applied on NN (no side information is provided)  

##Analytical_Approach:
callcross6_final.m: will take as input the dataset, parameters and directory name where you want to store the output of the model and will output the final trained model, test error, and prediction.  
mainfunction4_finaltrain.m: is called by callcross6_final, apply feed forward path and calculate the gradient, train the model and output the model

##Stochastic_Approach:
Test_example: will as input the dataset, parameters, and the outpout path name, and will set up the network structure, call the trainining function, compute the testing error and save the model and test error.  
nntrain: it calls the forward, backward pass, trains the model and outputs the final model. 